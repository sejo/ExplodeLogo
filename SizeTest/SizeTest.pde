import nervoussystem.obj.*;

ArrayList<Piece> pcs;
Piece pc;
final float XRANGE = 304.076;
final float PHYSICAL_XRANGE = 150;
final float SCALE_FACTOR = PHYSICAL_XRANGE/XRANGE;

PVector cam;

boolean record;

void setup(){
	size(600,600,P3D);
	pcs = new ArrayList<Piece>();

	pc = new Piece(304/2,100,0,36.365,SCALE_FACTOR);
	pc.select();

	pcs.add(pc);

	pcs.add(new Piece(304/2,100,0,17.678,SCALE_FACTOR));



	cam = new PVector(304/2,-100,-200);

	record = false;
}


void draw(){

	background(0);

	if(record){
		beginRecord("nervoussystem.obj.OBJExport","model.obj");
	}

	camera(cam.x, cam.y, cam.z, cam.x,cam.y,0, 0,-1,0);

	scale(SCALE_FACTOR);
	rotateX(PI);

	/*
	if(pcs.get(0).getScreenSize()>pcs.get(1).getScreenSize()){
		pcs.get(0).decZ();
	}
	*/

	for(Piece p : pcs){
		p.draw();
		if(p.selected){
			p.printResults();
		}

	}


	pcs.get(0).searchPositionAndSize(width/2,height/2,pcs.get(1).getScreenSize());

	if(record){
		endRecord();
		record = false;
	}

}

void keyPressed(){
	for(Piece p : pcs){
		switch(key){
			case 'a':
				p.decX();
			break;
			case 'd':
				p.incX();
			break;
			case 's':
				p.incY();
			break;
			case 'w':
				p.decY();
			break;
			case 'n':
				p.decZ();
			break;
			case 'm':
				p.incZ();
			break;
		}
	}
	switch(key){
		case 'r':
			record = true;
		break;
	}

	if(key==CODED){
		switch(keyCode){
			case DOWN:
				cam.z -= 10;
				println(cam.z);
			break;
			case UP:
				cam.z += 10;
				println(cam.z);
			break;
		}
	}


}
