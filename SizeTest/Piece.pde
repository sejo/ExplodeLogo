class Piece{
	PVector p;

	float screenx,screeny;
	float h;

	float screenSize;
	boolean selected;
	String results;

	float scaleFactor;


	Piece(float x, float y , float z, float hg, float sf){
		p = new PVector(x, y, z);

		h = hg;

		scaleFactor = sf;

		selected = false;
	}

	float getScreenSize(){
		return screenSize;
	}

	void draw(){

		strokeWeight(5);
		stroke(255);

		pushMatrix();
		evalTransformation();
		/*
		line(0,h/2,0,  0,-h/2,0);
		line(-h/2,0,0,  h/2,0,0);
		*/
		sphere(h/2);

		popMatrix();
	}

	void evalTransformation(){
		PVector p1 = new PVector(0,-h/2,0);
		PVector p2 = new PVector(0,h/2,0);

		translate(p.x,p.y,p.z);
		PVector top = screenV(p1);
		PVector bot = screenV(p2);

		PVector dist = new PVector();
		dist.set(top);
		dist.sub(bot);
		screenSize = dist.mag();
		generateResults();
		screenx = screenX(0,0,0);
		screeny = screenY(0,0,0);
		}

	void generateResults(){
		results = String.format("x:%f\ty:%f\tz:%f\tScreen size:%f\tScreen x:%f\tScreen y:%f",scaleFactor*p.x,scaleFactor*p.y,scaleFactor*p.z,screenSize,screenx,screeny);

	}

	void printResults(){
		println(results);
	}

	String getResults(){
		return results;
	}

	PVector screenV(PVector v){
		return new PVector(screenX(v.x,v.y,v.z),screenY(v.x,v.y,v.z),screenZ(v.x,v.y,v.z));	
	}

	PVector modelV(PVector v){
		return new PVector(modelX(v.x,v.y,v.z),modelY(v.x,v.y,v.z),modelZ(v.x,v.y,v.z));
	}

	void select(){
		selected = true;
	}

	void unselect(){
		selected = false;
	}

	boolean searchPositionAndSize(float tsx, float tsy, float tsize){
		boolean result = false;
		float difS = tsize-screenSize;
		float difX = tsx - screenx;
		float difY = tsy - screeny;

		float factorS = 0.1;
		float factorX = 0.1;
		float factorY = 0.1;

		float dz, dx, dy;

		if(abs(difS)>1){ // Move in Z to get target size
			dz = factorS*difS;	
			p.z += dz;
		}
		else if(abs(difX)>1){
			dx = factorX*difX;
			p.x += dx;
		}
		else if(abs(difY)>1){
			dy = factorY*difY;
			p.y += dy;
		}
		else{
			result = true;
		}


		return result;

	}


	void decX(){
		if(selected){
			p.x -= 1;
		}
	}

	void incX(){
		if(selected){
			p.x += 1;
		}
	}

	void decY(){
		if(selected){
			p.y -= 1;
		}
	}

	void incY(){
		if(selected){
			p.y += 1;
		}
	}

	void decZ(){
		if(selected){
			p.z -= 1;
		}
	}

	void incZ(){
		if(selected){
			p.z += 1;
		}
	}

	







}
