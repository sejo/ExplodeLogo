import nervoussystem.obj.*;

final float CYL_RADIUS = 8.586/2;
final float CYL_LENGTH = 20.203+4;

final float SPH_RAD_1 = 36.365/2;
final float SPH_RAD_2 = 30.810/2;
final float SPH_RAD_3 = 24.749/2;
final float SPH_RAD_4 = 17.678/2;
final float SPH_RAD_5 = 15.714/2;
final float SPH_RAD_6 = 10.664/2;

final float TOP_Y = 1052.36220;
final float XRANGE = 304.076;

final float PHYSICAL_XRANGE = 150;

final float SCALE_FACTOR = PHYSICAL_XRANGE/XRANGE;

final int TIPO_HORIZONTAL_CYL = 0;
final int TIPO_VERTICAL_CYL  = 1;
final int TIPO_SPH_1  = 2;
final int TIPO_SPH_2  = 3;
final int TIPO_SPH_3  = 4;
final int TIPO_SPH_4  = 5;
final int TIPO_SPH_5  = 6;
final int TIPO_SPH_6  = 7;

PShape s;
float angle;

ArrayList<Pieza> pzs;

ArrayList<Piece> pcs;
Piece pc;

boolean record;

PVector cam;

void setup(){
//	size(700,700,P3D);
	size(1420,1420,P3D);

	record = false;

	pzs = new ArrayList<Pieza>();

	pcs = new ArrayList<Piece>();
	
	addPiezasR();

	float tx, ty;

	float rad=0;
	float piezasize = 0;
	for(Pieza pz : pzs){
	//	pc = new Piece(width/2,height/2,0,30/SCALE_FACTOR,SCALE_FACTOR);

//		tx = (width-XRANGE)/2 + (pz.p.x); // Inverted
		tx = (width-XRANGE)/2 + (XRANGE-pz.p.x);
		ty = (height-XRANGE)/2 + realY(pz.p.y);
		switch(pz.tipo){
			case TIPO_SPH_1:
				rad = SPH_RAD_1;
				piezasize = 30;//30 works
			break;
			case TIPO_SPH_2:
				rad = SPH_RAD_2;
				piezasize = 22;//22 works
			break;
			case TIPO_SPH_3:
				rad = SPH_RAD_3;
				piezasize = 22; //22 works
			break;
			case TIPO_SPH_4:
				rad = SPH_RAD_4;
				piezasize = 20;
			break;
			case TIPO_SPH_5:
				rad = SPH_RAD_5;
				piezasize = 10;
			break;
			case TIPO_SPH_6:
				rad = SPH_RAD_6;
				piezasize = 10;
			break;

		}
		float scalingsize = 2.0/3;
		pc = new Piece(width/2,height/2,0,scalingsize*piezasize/SCALE_FACTOR,SCALE_FACTOR);
//		pc.setTargets(tx+rad, ty-rad, rad*2); // Inverted
		pc.setTargets(tx-rad, ty-rad, rad*2);

		pcs.add(pc);


	}




	cam = new PVector((PHYSICAL_XRANGE)/2,-(PHYSICAL_XRANGE/2),-250);

}

void draw(){
	background(0);
	fill(255);
	lights();

	if(record){
		beginRecord("nervoussystem.obj.OBJExport","model.obj");
	}

//	float fov = PI/3.0;
	float fov = radians(60);
	float cameraZ = (height/2.0) / tan(fov/2.0);
	perspective(fov, float(width)/float(height), cameraZ/10.0, cameraZ*10.0);
	camera(cam.x, cam.y, cam.z, cam.x,cam.y,0, 0,-1,0);

	sphereDetail(10);

	rotateX(PI);

	scale(SCALE_FACTOR);

	int count = 0;
	for(Piece p : pcs){
		p.draw();
		/*
		if(p.selected){
			p.printResults();
		}
		*/
		if(p.selected){
			count++;
			if(p.searchPositionAndSize()){
				p.unselect();
			}
		}
	}


	if(record){
		endRecord();
		for(Piece p : pcs){
			p.printModelP();
		}

		record = false;
		exit();
	}

	if(count==0){
		record = true;
	}


}

void keyPressed(){
	switch(key){
		case 'r':
			record = true;
		break;
		case 's':
			saveFrame();
		break;

		case CODED:
			switch(keyCode){
				case RIGHT:
					cam.x += 5;
				break;
				case LEFT:
					cam.x -= 5;
				break;
				case UP:
					cam.z += 5;
				break;
				case DOWN:
					cam.z -= 5;
				break;

			}
		break;

	}

}

void sph(float trx, float ypty, float z, float radius){
	float x = trx + radius;
	float y = realY(ypty) - radius;

	pushMatrix();
	translate(x,y,z);
	sphere(radius);

	popMatrix();
}



float realY(float y){
	return TOP_Y - y;
}	


void addPiezasR(){
	// Spheres 1
	pzs.add(new Pieza(TIPO_SPH_1, 54.112,941.437, SPH_RAD_1));
	pzs.add(new Pieza(TIPO_SPH_1, 119.762,982.409, SPH_RAD_1));
	pzs.add(new Pieza(TIPO_SPH_1, 195.523,963.216,  SPH_RAD_1));
	pzs.add(new Pieza(TIPO_SPH_1, 236.434,896.061 , SPH_RAD_1));
	pzs.add(new Pieza(TIPO_SPH_1, 218.252,820.785 , SPH_RAD_1));
	pzs.add(new Pieza(TIPO_SPH_1, 151.076,779.873 , SPH_RAD_1));
	pzs.add(new Pieza(TIPO_SPH_1, 75.820,798.582 , SPH_RAD_1));
	pzs.add(new Pieza(TIPO_SPH_1, 35.414,865.252 , SPH_RAD_1));

	// Spheres 2
	pzs.add(new Pieza(TIPO_SPH_2, 21.019,907.910, SPH_RAD_2));
	pzs.add(new Pieza(TIPO_SPH_2, 72.790,984.702, SPH_RAD_2));
	pzs.add(new Pieza(TIPO_SPH_2, 162.946,1001.855, SPH_RAD_2));
	pzs.add(new Pieza(TIPO_SPH_2, 239.212,950.842, SPH_RAD_2));
	pzs.add(new Pieza(TIPO_SPH_2, 256.385,858.433, SPH_RAD_2));
	pzs.add(new Pieza(TIPO_SPH_2, 204.867,782.146, SPH_RAD_2));
	pzs.add(new Pieza(TIPO_SPH_2, 114.438,764.994, SPH_RAD_2));
	pzs.add(new Pieza(TIPO_SPH_2, 38.192,817.502, SPH_RAD_2));
	
	// Spheres 3
	pzs.add(new Pieza(TIPO_SPH_3, 107.114,946.084, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 159.936,953.367, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 201.331,921.547, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 207.392,869.019, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 176.078,827.603, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 122.540,820.532, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 81.628,853.362, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 75.567,905.385, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 1.806,870.050, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 31.626,975.086, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 124.560,1027.613, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 228.606,997.814, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 281.134,902.860, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 251.819,799.319, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 156.885,746.286, SPH_RAD_3));
	pzs.add(new Pieza(TIPO_SPH_3, 53.344,775.601, SPH_RAD_3));

	// Spheres 4
	pzs.add(new Pieza(TIPO_SPH_4, 13.948,837.200, SPH_RAD_4));
	pzs.add(new Pieza(TIPO_SPH_4, 15.443,946.296, SPH_RAD_4));
	pzs.add(new Pieza(TIPO_SPH_4, 92.235,1022.563, SPH_RAD_4));
	pzs.add(new Pieza(TIPO_SPH_4, 201.331,1020.058, SPH_RAD_4));
	pzs.add(new Pieza(TIPO_SPH_4, 276.083,943.266, SPH_RAD_4));
	pzs.add(new Pieza(TIPO_SPH_4, 275.073,833.664, SPH_RAD_4));
	pzs.add(new Pieza(TIPO_SPH_4, 196.806,759.418, SPH_RAD_4));
	pzs.add(new Pieza(TIPO_SPH_4, 89.689,760.933, SPH_RAD_4));

	// Spheres 5
	// From outer to inner circle
	pzs.add(new Pieza(TIPO_SPH_5, 139.886,1026.611, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 237.846,992.235, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 283.194,895.957, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 247.728,798.082, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 153.314,754.807, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 54.657,790.348, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 9.509,884.713, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 45.496,984.325, SPH_RAD_5));

	pzs.add(new Pieza(TIPO_SPH_5, 159.568,1018.385, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 244.445,971.950, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 272.838,877.309, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 227.492,791.336, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 134.018,763.900, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 46.807,810.182, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 19.223,903.705, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 66.069,989.961, SPH_RAD_5));

	pzs.add(new Pieza(TIPO_SPH_5, 179.189,1001.382, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 246.624,944.776, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 255.255,857.808, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 199.781,789.258, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 112.639,782.959, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 45.055,838.087, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 37.692,924.995, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 93.618,992.227, SPH_RAD_5));

	pzs.add(new Pieza(TIPO_SPH_5, 194.006,986.036, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 246.778,923.445, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 240.168,842.727, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 178.464,790.058, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 96.910,797.368, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 45.296,859.417, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 52.660,940.194, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 114.922,993.151, SPH_RAD_5));

	pzs.add(new Pieza(TIPO_SPH_5, 205.585,957.399, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 235.021,893.161, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 210.897,829.825, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 148.235,802.882, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 86.967,826.476, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 57.790,887.724, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 79.478,951.912, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 141.490,982.361, SPH_RAD_5));

	pzs.add(new Pieza(TIPO_SPH_5, 204.985,936.075, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 218.985,879.094, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 189.571,830.304, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 133.036,817.850, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 86.218,847.795, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 72.968,902.714, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 100.782,950.835, SPH_RAD_5));
	pzs.add(new Pieza(TIPO_SPH_5, 155.943,966.672, SPH_RAD_5));


	// Spheres 6
	// From outer to inner circle
	pzs.add(new Pieza(TIPO_SPH_6, 152.098,1026.096, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 244.383,985.433, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 281.624,889.192, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 240.956,796.528, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 146.297,760.801, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 52.597,801.932, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 15.811,896.663, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 57.527,990.419, SPH_RAD_6));

	pzs.add(new Pieza(TIPO_SPH_6, 189.406,997.280, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 250.152,937.198, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 251.287,852.528, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 192.168,791.234, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 107.082,791.628, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 46.765,850.733, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 46.649,935.377, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 106.287,995.171, SPH_RAD_6));

	pzs.add(new Pieza(TIPO_SPH_6, 208.756,949.791, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 230.559,888.320, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 203.294,831.648, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 142.904,811.840, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 88.208,839.073, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 66.856,898.016, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 92.148,954.856, SPH_RAD_6));
	pzs.add(new Pieza(TIPO_SPH_6, 151.550,978.080, SPH_RAD_6));

}
